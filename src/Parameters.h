//
// Created by jimmy on 02/10/17.
//

/**
 *  Class Parameters
 *
 *  Cette classe est utilisé pour gérer les différents paramètres d'entré
 *
 */

#ifndef POPULATION_PARAMETERS_H
#define POPULATION_PARAMETERS_H

#include <string>
#include <vector>
#include <ostream>

class Parameters {

public:
    Parameters(bool dynamic, std::string file);
    Parameters(unsigned long n, unsigned long ne, unsigned long na, unsigned long N0,
               double Bmax, double d, unsigned long n_rep, double mu, double t_max,
               int log10Pextlim, unsigned long nbt, double so, unsigned long nb_time_steps,
               double Es, double dtprint, unsigned long natba);
    virtual ~Parameters() = default;

    unsigned long getN() const;
    unsigned long getNe() const;
    unsigned long getNa() const;
    unsigned long getN0() const;
    double getBmax() const;
    double getD() const;
    unsigned long getN_rep() const;
    double getMu() const;
    double getT_max() const;
    int getLog10Pextlim() const;
    unsigned long getNbt() const;
    double getSo() const;
    unsigned long getNb_time_steps() const;
    double getEs() const;
    double getDtprint() const;
    unsigned long getNatba() const;

private:

    void init(bool dynamic, std::vector<std::string> params);

    unsigned long m_n;
    unsigned long m_ne;
    unsigned long m_na;
    unsigned long m_N0;
    double m_Bmax;
    double m_d;
    unsigned long m_n_rep;
    double m_mu;
    double m_t_max;
    int m_log10Pextlim;
    unsigned long m_nbt;
    double m_so;
    unsigned long m_nb_time_steps;
    double m_Es;
    double m_dtprint;
    unsigned long m_natba;

public:
    friend std::ostream &operator<<(std::ostream &os, const Parameters &parameters);
};


#endif //POPULATION_PARAMETERS_H
