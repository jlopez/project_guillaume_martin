//
// Created by jimmy on 03/10/17.
//

#include "Rescuers.h"

Rescuers::Rescuers(unsigned long size) {
    m_P.resize(size);
    m_R.resize(size);
    m_T.resize(size);
    m_NM.resize(size);
    m_NPC.resize(size);
}

unsigned long Rescuers::getP(unsigned long index) const {
    return m_P.at(index);
}

double Rescuers::getR(unsigned long index) const {
    return m_R.at(index);
}

double Rescuers::getT(unsigned long index) const {
    return m_T.at(index);
}

unsigned long Rescuers::getNM(unsigned long index) const {
    return m_NM.at(index);
}

double Rescuers::getNPC(unsigned long index) const {
    return m_NPC.at(index);
}

void Rescuers::setP(unsigned long index, unsigned long value) {
    m_P[index] = value;
}

void Rescuers::setR(unsigned long index, double value) {
    m_R[index] = value;
}

void Rescuers::setT(unsigned long index, double value) {
    m_T[index] = value;
}

void Rescuers::setNM(unsigned long index, unsigned long value) {
    m_NM[index] = value;
}

void Rescuers::setNPC(unsigned long index, double value) {
    m_NPC[index] = value;
}

NumericVector Rescuers::getVectorP(unsigned long size) const {
    NumericVector vec(size);
    for (unsigned long i = 1; i < size; i++) {
        vec(i-1) = m_P.at(i);
    }
    return vec;
}

NumericVector Rescuers::getVectorR(unsigned long size) const {
  NumericVector vec(size);
  for (unsigned long i = 1; i < size; i++) {
    vec(i-1) = m_R.at(i);
  }
  return vec;
}

NumericVector Rescuers::getVectorT(unsigned long size) const {
  NumericVector vec(size);
  for (unsigned long i = 1; i < size; i++) {
    vec(i-1) = m_T.at(i);
  }
  return vec;
}

NumericVector Rescuers::getVectorNM(unsigned long size) const {
  NumericVector vec(size);
  for (unsigned long i = 1; i < size; i++) {
    vec(i-1) = m_NM.at(i);
  }
  return vec;
}

NumericVector Rescuers::getVectorNPC(unsigned long size) const {
  NumericVector vec(size);
  for (unsigned long i = 1; i < size; i++) {
    vec(i-1) = m_NPC.at(i);
  }
  return vec;
}
