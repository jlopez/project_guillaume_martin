//
// Created by jimmy on 05/10/17.
//

/**
 *  Class InputParser
 *
 *  Cette classe est utilisé pour parser les paramètres d'entrées qui sont dans un fichier texte
 *
 */


#ifndef POPULATION_INPUTPARSER_H
#define POPULATION_INPUTPARSER_H

#include <string>
#include <vector>

class InputParser {
public:
    InputParser (int &argc, char **argv);
    virtual ~InputParser() = default;

    const std::string& getCmdOption(const std::string &option) const;
    bool cmdOptionExists(const std::string &option) const;

private:
    std::vector <std::string> m_tokens;
};


#endif //POPULATION_INPUTPARSER_H
