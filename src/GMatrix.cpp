//
// Created by jimmy on 02/10/17.
//

#include "GMatrix.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
//#include <gsl/gsl_matrix.h>
//#include <gsl/gsl_vector.h>
#include <cmath>

GMatrix::GMatrix(unsigned long rows, unsigned long cols) {
    /*m_matrix = gsl_matrix_alloc(rows, cols);

    for (unsigned long i = 0; i < rows; i++) {
        for (unsigned long j = 0; j < cols; j++) {
            setValue(i, j, 0);
        }
    }*/
    m_sizer = rows;
    m_sizec = cols;
    m2_matrix = new double*[rows];
    for (unsigned long i = 0; i < rows; i++) {
      m2_matrix[i] = new double[cols];
      for (unsigned long j = 0; j < cols; j++) {
        setValue(i, j, 0.0);
      }
    }
}

GMatrix::GMatrix(NumericMatrix mat) {
    /*m_matrix = gsl_matrix_alloc(mat.rows(), mat.cols());

    unsigned long x = 0;

    for (unsigned long i = 0; i < mat.rows(); i++) {
        for (unsigned long j = 0; j < mat.cols(); j++) {
            setValue(i, j, mat[x]);

            x += 1;
        }
    }*/
    m_sizer = mat.rows();
    m_sizec = mat.cols();

    unsigned long x = 0;

    m2_matrix = new double*[m_sizer];
    for (unsigned long i = 0; i < m_sizer; i++) {
      m2_matrix[i] = new double[m_sizec];
      for (unsigned long j = 0; j < m_sizec; j++) {
        setValue(i, j, mat[x]);
        x += 1;
      }
    }
}

GMatrix::GMatrix(NumericVector vec) {
    /*m_matrix = gsl_matrix_alloc(vec.size(), 1);
    for (unsigned long i = 0; i < vec.size(); i++) {
      setValue(i, 0, vec[i]);
    }*/
    m_sizer = vec.size();
    m_sizec = 1;
    m2_matrix = new double*[m_sizer];
    for (unsigned long i = 0; i < m_sizer; i++) {
      m2_matrix[i] = new double[m_sizec];
      setValue(i, 0, vec[i]);
    }
}

GMatrix::GMatrix(std::string file, unsigned long rows, unsigned long cols, bool reverse) {
/*
    m_matrix = gsl_matrix_alloc(rows, cols);

    if(reverse) {
        std::ifstream infile(file);
        std::string line;
        unsigned long i = 0;
        while (getline(infile,line)) // Read a line
        {
            std::istringstream iss(line);
            double value;
            unsigned long j = 0;
            while (iss >> value) { // Read columns
                gsl_matrix_set(m_matrix, j, i, value);
                j++;
            }
            i++;
        }

        infile.close();
    } else {
        std::ifstream infile(file);
        std::string line;
        unsigned long i = 0;
        while (getline(infile,line)) // Read a line
        {
            std::istringstream iss(line);
            double value;
            unsigned long j = 0;
            while (iss >> value) { // Read columns
                gsl_matrix_set(m_matrix, i, j, value);
                j++;
            }
            i++;
        }

        infile.close();
    }*/
      m_sizer = rows;
        m_sizec = cols;
        m2_matrix = new double*[rows];
        for (unsigned long i = 0; i < rows; i++) {
          m2_matrix[i] = new double[cols];
          for (unsigned long j = 0; j < cols; j++) {
            setValue(i, j, 0.0);
          }
        }

        std::ifstream infile(file);
        std::string line;
        unsigned long i = 0;
        while (getline(infile,line)) // Read a line
        {
          std::istringstream iss(line);
          double value;
          unsigned long j = 0;
          while (iss >> value) { // Read columns
            if(reverse) {
              setValue(j, i, value);
              //gsl_matrix_set(m_matrix, j, i, value);
            } else {
              setValue(i, j, value);
              //gsl_matrix_set(m_matrix, i, j, value);
            }
            j++;
          }
          i++;
        }

        infile.close();
}

GMatrix::~GMatrix() {
    //gsl_matrix_free(m_matrix);
    for(int i = 0; i < m_sizer; ++i) {
      delete [] m2_matrix[i];
    }
    delete [] m2_matrix;
}

double GMatrix::getValue(unsigned long i, unsigned long j) const {
    //return gsl_matrix_get(m_matrix, i, j);
    return m2_matrix[i][j];
}

void GMatrix::setValue(unsigned long i, unsigned long j, double value) {
    //gsl_matrix_set(m_matrix, i, j, value);
    m2_matrix[i][j] = value;
}

/*gsl_matrix *GMatrix::getMatrix() const {
    return m_matrix;
}*/

void GMatrix::sub(const GMatrix *gMatrix) {
    //gsl_matrix_sub(m_matrix, gMatrix->getMatrix());
    for (unsigned long i = 0; i < m_sizer; i++) {
      for (unsigned long j = 0; j < m_sizec; j++) {
        m2_matrix[i][j] -= gMatrix->getValue(i, j);
      }
    }
}

double GMatrix::trace() const {
    //gsl_vector_view v = gsl_matrix_diagonal(m_matrix);

    //unsigned long size = std::min(m_matrix->size1, m_matrix->size2);
    unsigned long size = std::min(m_sizer, m_sizec);

    double sum = 0.0;

    for (unsigned long i = 0; i < size; i++) {
      //sum += gsl_vector_get(&v.vector, i);
      sum += getValue(i, i);
    }

    return sum;
}

GMatrix GMatrix::getTranspose() const {
    //gsl_matrix_transpose(m_matrix);
    GMatrix matrix(m_sizec, m_sizer);

    for (unsigned long i = 0; i < m_sizer; i++) {
      for (unsigned long j = 0; j < m_sizec; j++) {
        matrix.setValue(j, i, m2_matrix[i][j]);
      }
    }
    return matrix;
}

GMatrix* GMatrix::mult(double val) {
    /*for (unsigned long i = 0; i < m_matrix->size1; ++i) {
        for (unsigned long j = 0; j <m_matrix->size2 ; ++j) {
            double v = getValue(i, j) * val;
            setValue(i, j, v);
        }
    }*/
    for (unsigned long i = 0; i < m_sizer; ++i) {
      for (unsigned long j = 0; j < m_sizec; ++j) {
        double v = getValue(i, j) * val;
        setValue(i, j, v);
      }
    }
    return this;
}

GMatrix* GMatrix::div(double val) {
    /*for (unsigned long i = 0; i < m_matrix->size1; ++i) {
        for (unsigned long j = 0; j <m_matrix->size2 ; ++j) {
            double v = val / getValue(i, j);
            setValue(i, j, v);
        }
    }*/
    for (unsigned long i = 0; i < m_sizer; ++i) {
      for (unsigned long j = 0; j < m_sizec; ++j) {
        double v = val / getValue(i, j);
        setValue(i, j, v);
      }
    }
    return this;
}

GMatrix* GMatrix::sqrt() {
    /*for (unsigned long i = 0; i < m_matrix->size1; ++i) {
        for (unsigned long j = 0; j <m_matrix->size2 ; ++j) {
            double v = std::sqrt(getValue(i, j));
            setValue(i, j, v);
        }
    }*/
    for (unsigned long i = 0; i < m_sizer; ++i) {
      for (unsigned long j = 0; j < m_sizec; ++j) {
        double v = std::sqrt(getValue(i, j));
        setValue(i, j, v);
      }
    }
    return this;
}

unsigned long GMatrix::getRowSize() const {
    //return m_matrix->size1;
    return m_sizer;
}

unsigned long GMatrix::getColSize() const {
    //return m_matrix->size2;
    return m_sizec;
}



