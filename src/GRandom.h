//
// Created by jimmy on 03/10/17.
//

/**
 *  Class GRandom
 *
 *  Cette classe est utilisé pour encapsuler le random de gsl
 *
 */

#ifndef POPULATION_GRANDOM_H
#define POPULATION_GRANDOM_H

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>
#include <random>

class GRandom {

public:
    GRandom();
    virtual ~GRandom();

    virtual double exponential(double mu);
    virtual double flat(double a, double b);
    virtual unsigned long poisson(double mu);

private:
    //gsl_rng* m_rgsl;
    std::default_random_engine m_generator;
};


#endif //POPULATION_GRANDOM_H
