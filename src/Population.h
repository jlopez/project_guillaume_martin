//
// Created by jimmy on 02/10/17.
//

#ifndef POPULATION_POPULATION_H
#define POPULATION_POPULATION_H

#include "GMatrix.h"
#include <vector>
#include "GRandom.h"

/**
 *  Class Population
 *
 *  Cette classe est utilisé pour gérer une population
 *
 */

class Population {

public:
    Population(unsigned long rows, unsigned long cols);
    virtual ~Population();

    double getValue(unsigned long i, unsigned long j);
    void setValue(unsigned long i, unsigned long j, double value);

    double getBg(unsigned long index);
    void setBg(unsigned long index, double value);

    double getrg(unsigned long index);
    void setrg(unsigned long index, double value);
    double getMaxrg();

    unsigned long getNg(unsigned long index);
    void setNg(unsigned long index, unsigned long value);

    double getTg(unsigned long index);
    void setTg(unsigned long index, double value);

    double getnm(unsigned long index);
    void setnm(unsigned long index, unsigned long value);

    double getBp() const;
    void setBp(double Bp);

    unsigned long getNp() const;
    void setNp(unsigned long Np);

    double getNpc() const;
    void setNpc(double Npc);

    unsigned long getng() const;
    void setng(unsigned long ng);

    virtual double compute_Er();
    virtual void evalbirthrate(GMatrix* Sb, double BMax, const GMatrix* x0, unsigned long n, unsigned long nl);
    virtual void birth(GRandom* rnd, double t, double u, GMatrix *A, double mu, unsigned long na, GMatrix *Sb, double Bmax, GMatrix *x0, unsigned long n, double d, double dt);
    virtual void death(GRandom* rnd, double u,  double d, unsigned long n, double dt);
    virtual unsigned long compute_imaxr(unsigned long max);

private:
    GMatrix* m_matrix;
    std::vector<double> m_Bg; // Bg[i] = birth rate genotype i
    std::vector<double> m_rg; // rg[i] = growth rate genotype i
    std::vector<unsigned long> m_Ng; // Ng[i] = number of indiv with genotype i
    std::vector<double> m_Tg; //Tg[i] = time at which genotype i appears
    std::vector<unsigned long> m_nm; // nombre de mutations cumulées
    double m_Bp;     // population birth rate
    unsigned long m_Np;      // population size
    double m_Npc;      // cumulative population size
    unsigned long m_ng; // nombre de génotypes différents
};


#endif //POPULATION_POPULATION_H
