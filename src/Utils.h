//
// Created by jimmy on 03/10/17.
//

/**
 *  Class Utils
 *
 *  Cette classe stocke des fonctions utiles
 *
 */

#ifndef POPULATION_UTILS_H
#define POPULATION_UTILS_H

#include <iostream>
#include <string>
//#include <gsl/gsl_blas.h>
#include "GMatrix.h"


class Utils {

public:

    static void debug(const std::string& message) {
        std::cout << message << std::endl;
    }

    static void dgemmTN(GMatrix* A, GMatrix* B, GMatrix* C) {
        //gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A->getMatrix(), B->getMatrix(), 0.0, C->getMatrix());
        GMatrix T = A->getTranspose();
        for (unsigned long i = 0; i < C->getRowSize(); ++i) {
          for (unsigned long j = 0; j < C->getColSize(); ++j) {
            double val = 0.0;
            for (unsigned long k = 0; k < T.getColSize(); ++k) {
              val += T.getValue(i, k) * B->getValue(k, j);
            }
            C->setValue(i, j, val);
          }
        }
    }

    static void dgemmNN(GMatrix* A, GMatrix* B, GMatrix* C) {
        //gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A->getMatrix(), B->getMatrix(), 0.0, C->getMatrix());
        for (unsigned long i = 0; i < C->getRowSize(); ++i) {
          for (unsigned long j = 0; j < C->getColSize(); ++j) {
            double val = 0.0;
            for (unsigned long k = 0; k < A->getColSize(); ++k) {
              val += A->getValue(i, k) * B->getValue(k, j);
            }
            C->setValue(i, j, val);
          }
        }
    }

    /**
     * Permet d'avoir le bon séparateur en fonction de l'os
     */
    static std::string separator() {
        #ifdef _WIN32
                return "\\";
        #else
                return "/";
        #endif
    }

    /**
     * Permet de montrer le help du programme
     */
    static void showUsage() {
        std::cerr << "Usage: \n"
                  << "\t-h\t\tShow this help message\n"
                  << "\t-i INPUT\tSpecify the input path\n"
                  << "\t-o OUTPUT\tSpecify the output path\n"
                  << "\t-g MAX_GENO\tSpecify the max genotype\toptional & default 500\n"
                  << "\t-m MATRIX\tSpecify the input matrix path\tpath to matA.dat, matSb.dat & vectxo.dat\n"
                  << "\t-r RATE\tIf you want the vector of positive rate\toptional\n"
                  << std::endl;

        exit(-1);
    }

    /**
     * Permmet de multiplier de matrice
     * return : une nouvelle GMatrix qui est le produit des deux autres
     */
    static GMatrix* multiplyMatrix(GMatrix* ma, GMatrix* mb) {
        GMatrix* mc = new GMatrix(ma->getRowSize(), mb->getColSize());

        if(ma->getColSize() != mb->getRowSize()) {
            return mc;
        }

        for (unsigned long i = 0; i < ma->getRowSize(); i++) {
            for (unsigned long j = 0; j < mb->getColSize(); j++) {
                double sum = 0;
                for (unsigned long k = 0; k < mb->getRowSize(); k++) {
                    sum += ma->getValue(i, k) * mb->getValue(k, j);
                }
                mc->setValue(i, j, sum);
            }
        }
    }

};

#endif //POPULATION_UTILS_H
