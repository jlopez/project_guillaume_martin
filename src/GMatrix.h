//
// Created by jimmy on 02/10/17.
//

/**
 *  Class GMatrix
 *
 *  Cette classe est utilisé pour encapsuler une matrice gsl
 *
 */

#ifndef POPULATION_INPUTMATRIX_H
#define POPULATION_INPUTMATRIX_H

#include <string>
//#include<gsl/gsl_matrix.h>
#include "Parameters.h"

#include <Rcpp.h>
using namespace Rcpp;

class GMatrix {

public:
    GMatrix(unsigned long rows, unsigned long cols);
    GMatrix(std::string file, unsigned long rows, unsigned long cols, bool reverse);
    GMatrix(NumericMatrix mat);
    GMatrix(NumericVector vec);
    virtual ~GMatrix();

    //gsl_matrix *getMatrix() const;

    double getValue(unsigned long i, unsigned long j) const;
    void setValue(unsigned long i, unsigned long j, double value);
    virtual void sub(const GMatrix* gMatrix);
    double trace() const;
    GMatrix getTranspose() const;
    GMatrix* mult(double val);
    GMatrix* div(double val);
    GMatrix* sqrt();
    unsigned long getRowSize() const;
    unsigned long getColSize() const;

private:
    //gsl_matrix* m_matrix;
    double ** m2_matrix;
    unsigned long m_sizer;
    unsigned long m_sizec;

};


#endif //POPULATION_INPUTMATRIX_H
