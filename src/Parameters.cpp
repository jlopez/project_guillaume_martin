//
// Created by jimmy on 02/10/17.
//

#include "Parameters.h"
#include <fstream>
#include <iostream>
#include <sstream>

Parameters::Parameters(bool dynamic, const std::string file) {

    std::ifstream inputFile;
    std::string line;
    std::vector<std::string> params;

    inputFile.open(file);
    if(inputFile) {
        while (inputFile >> line)
        {
            if(line.at(0) != '#') {
                params.push_back(line);
            }
        }

        if(dynamic) {
            if(params.size() != 16) {
                //TODO error
                exit(1);
            }
        } else {
            if(params.size() != 14) {
                //TODO error
                //exit(1);
            }
        }

        init(dynamic, params);

        inputFile.close();
    } else {
        //TODO error file not found
        exit(1);
    }
}

Parameters::Parameters(unsigned long n, unsigned long ne, unsigned long na, unsigned long N0,
           double Bmax, double d, unsigned long n_rep, double mu, double t_max,
           int log10Pextlim, unsigned long nbt, double so, unsigned long nb_time_steps,
           double Es, double dtprint, unsigned long natba)
{
    m_n = n;
    m_ne = ne;
    m_na = na;
    m_N0 = N0;
    m_Bmax = Bmax;
    m_d = d;
    m_n_rep = n_rep;
    m_mu = mu;
    m_t_max = t_max;
    m_log10Pextlim = log10Pextlim;
    m_nbt = nbt;
    m_so = so;
    m_nb_time_steps = nb_time_steps;
    m_Es = Es;
    m_dtprint = dtprint;
    m_natba = natba;
}

void Parameters::init(bool dynamic, std::vector<std::string> params) {
    m_n = std::stoul(params.at(0));
    m_ne = std::stoul(params.at(1));
    m_na = std::stoul(params.at(2));
    m_N0 = std::stoul(params.at(3));
    m_Bmax = std::stod(params.at(4));
    m_d = std::stod(params.at(5));
    m_n_rep = std::stoul(params.at(6));
    m_mu = std::stod(params.at(7));
    m_t_max = std::stod(params.at(8));
    m_log10Pextlim = std::stoi(params.at(9));
    m_nbt = std::stoul(params.at(10));
    m_so = std::stod(params.at(11));
    m_nb_time_steps = std::stoul(params.at(12));
    m_Es = std::stod(params.at(13));
    if(dynamic) {
        m_dtprint = std::stod(params.at(14));
        m_natba = std::stoul(params.at(15));
    } else {
        m_dtprint = 0.0;
        m_natba = 0;
    }
}

unsigned long Parameters::getN() const {
    return m_n;
}

unsigned long Parameters::getNe() const {
    return m_ne;
}

unsigned long Parameters::getNa() const {
    return m_na;
}

unsigned long Parameters::getN0() const {
    return m_N0;
}

double Parameters::getBmax() const {
    return m_Bmax;
}

double Parameters::getD() const {
    return m_d;
}

unsigned long Parameters::getN_rep() const {
    return m_n_rep;
}

double Parameters::getMu() const {
    return m_mu;
}

double Parameters::getT_max() const {
    return m_t_max;
}

int Parameters::getLog10Pextlim() const {
    return m_log10Pextlim;
}

unsigned long Parameters::getNbt() const {
    return m_nbt;
}

double Parameters::getSo() const {
    return m_so;
}

unsigned long Parameters::getNb_time_steps() const {
    return m_nb_time_steps;
}

double Parameters::getEs() const {
    return m_Es;
}

double Parameters::getDtprint() const {
    return m_dtprint;
}

unsigned long Parameters::getNatba() const {
    return m_natba;
}

std::ostream &operator<<(std::ostream &os, const Parameters &parameters) {
    os.precision(16);
    os << "Parameters :\n" << "m_n: " << parameters.m_n << " \nm_ne: " << parameters.m_ne << " \nm_na: " << parameters.m_na << " \nm_N0: "
       << parameters.m_N0 << " \nm_Bmax: " << parameters.m_Bmax << " \nm_d: " << parameters.m_d << " \nm_n_rep: "
       << parameters.m_n_rep << " \nm_mu: " << parameters.m_mu << " \nm_t_max: " << parameters.m_t_max
       << " \nm_log10Pextlim: " << parameters.m_log10Pextlim << " \nm_nbt: " << parameters.m_nbt << " \nm_so: "
       << parameters.m_so << " \nm_nb_time_steps: " << parameters.m_nb_time_steps << " \nm_Es: " << parameters.m_Es
       << " \nm_dtprint: " << parameters.m_dtprint << " \nm_natba: " << parameters.m_natba;
    return os;
}
