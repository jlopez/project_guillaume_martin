//
// Created by jimmy on 03/10/17.
//

/**
 *  Class Rescuers
 *
 *  Cette classe est utilisé pour gérer les matrices résultats
 *
 */

#ifndef POPULATION_RESCUERS_H
#define POPULATION_RESCUERS_H

#include <vector>
#include <Rcpp.h>
using namespace Rcpp;

class Rescuers {

public:
    Rescuers(unsigned long size);
    virtual ~Rescuers() = default;

    unsigned long getP(unsigned long index) const;
    double getR(unsigned long index) const;
    double getT(unsigned long index) const;
    unsigned long getNM(unsigned long index) const;
    double getNPC(unsigned long index) const;

    void setP(unsigned long index, unsigned long value);
    void setR(unsigned long index, double value);
    void setT(unsigned long index, double value);
    void setNM(unsigned long index, unsigned long value);
    void setNPC(unsigned long index, double value);

    NumericVector getVectorP(unsigned long size) const;
    NumericVector getVectorR(unsigned long size) const;
    NumericVector getVectorT(unsigned long size) const;
    NumericVector getVectorNM(unsigned long size) const;
    NumericVector getVectorNPC(unsigned long size) const;


private:
    std::vector<unsigned long> m_P;
    std::vector<double> m_R;
    std::vector<double> m_T;
    std::vector<unsigned long> m_NM;
    std::vector<double> m_NPC;
};


#endif //POPULATION_RESCUERS_H
