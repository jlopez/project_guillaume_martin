#include <iostream>
#include "Population.h"
#include "Utils.h"
#include "Rescuers.h"
#include <cmath>
#include <fstream>
#include "InputParser.h"

#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
List Rrun(unsigned long max_geno, unsigned long n, unsigned long ne, unsigned long na, unsigned long N0,
          double Bmax, double d, unsigned long n_rep, double mu, double t_max,
          int log10Pextlim, unsigned long nbt, double so, unsigned long nb_time_steps,
          double Es, double dtprint, unsigned long natba,
          NumericMatrix RSb, NumericMatrix RA, NumericVector Rxo, bool have_rate, bool have_rescuers, std::string output_path, bool verbose) {
  if(verbose) {
    /*std::cout << "max_geno : " << max_geno << std::endl;
    std::cout << "n : " << n << std::endl;
    std::cout << "ne : " << ne << std::endl;
    std::cout << "na : " << na << std::endl;
    std::cout << "N0 : " << N0 << std::endl;
    std::cout << "Bmax : " << Bmax << std::endl;
    std::cout << "d : " << d << std::endl;
    std::cout << "n_rep : " << n_rep << std::endl;
    std::cout << "mu : " << mu << std::endl;*/
    std::cout << "t_max : " << t_max << std::endl;
    /*std::cout << "log10Pextlim : " << log10Pextlim << std::endl;
    std::cout << "nbt : " << nbt << std::endl;
    std::cout << "so : " << so << std::endl;*/
    std::cout << "nb_time_steps : " << nb_time_steps << std::endl;
    /*std::cout << "Es : " << Es << std::endl;
    std::cout << "dtprint : " << dtprint << std::endl;
    std::cout << "natba : " << natba << std::endl;
    std::cout << "have_rate : " << have_rate << std::endl;*/

  }

  bool save_output_simulation = true;
  if(output_path == "") {
    save_output_simulation = false;
  }

  if(!have_rescuers) {
    save_output_simulation = false;
  }

  GMatrix Sb(RSb);
  GMatrix A(RA);
  GMatrix x0(Rxo);

  Parameters param(n, ne, na, N0, Bmax, d, n_rep, mu, t_max, log10Pextlim, nbt, so, nb_time_steps, Es, dtprint, natba);

  GRandom grd;

  const unsigned long nrnt = param.getNb_time_steps() * param.getN_rep();

  Rescuers rescuers(param.getN_rep());

  double pp = 0;

  for(unsigned long i_rep = 0; i_rep < param.getN_rep(); i_rep++) {
      if(verbose) {
         double d = ((double)i_rep/(double)param.getN_rep())*100.0;
        if(d >= pp) {
          std::cout << "running : " << pp << "%" << std::endl;
          pp += 5;
        }
      }

      Population pop(max_geno, param.getN());
      pop.setng(1);
      pop.setNg(0, param.getN0());
      pop.setTg(0, 0.0);
      pop.evalbirthrate(&Sb, param.getBmax(), &x0, param.getN(), 0);
      pop.setrg(0, pop.getBg(0) - param.getD());
      pop.setNp(param.getN0());
      pop.setNpc(0.0);
      pop.setBp(pop.getBg(0)*param.getN0());
      pop.setnm(0, 0);
      double Er = pop.compute_Er();

      // initialize times
      double t = 0.0; // current time
      unsigned int nb = 0; // number of birth since the last rescue test

      rescuers.setP(i_rep, 1);

      unsigned long imaxr = 0;

      // simulation loop on time
      while( t < param.getT_max()) {
        // total rate of events and time at which next event occurs
        double total_rate = pop.getBp() + param.getD() * pop.getNp();
        double dt = grd.exponential(total_rate);
        t += dt;

        double u = grd.flat(0.0, total_rate);

        if(u < pop.getBp()) {

          if (pop.getng() >= max_geno) {
            std::cout << "ERROR: max number of genotypes reached\nIncrease MAX_GENO constant\n" << std::endl;
            //TODO error
          } else {
            pop.birth(&grd, t, u, &A, param.getMu(), param.getNa(), &Sb, param.getBmax(), &x0, param.getN(), param.getD(), dt);
            nb++;

            if(nb > param.getNbt()) { // if nbt birth, test is there is a rescue
              nb = 0;
              Er = pop.compute_Er();
              if(Er > 0) {
                if(pop.getNp() * log(param.getD() / (Er+param.getD())) < param.getLog10Pextlim()) {
                  break;
                }
              }
            }
          }

        } else {
          pop.death(&grd, u, param.getD(), param.getN(), dt);
          if(pop.getNp() == 0) {
            rescuers.setP(i_rep, 0);
            break;
          }
        }
      } // end of simulation loop on time

      // test if simul stops because t_max reached (in this case, failed simul)
      if (t >= param.getT_max()) {
        std::cerr << "ERROR: t_max reached\nIncrease factsecuext parameter\n" << std::endl;
        //TODO error
        //exit(1);
      } else {

          imaxr = pop.compute_imaxr(imaxr);

          if(pop.getNp() != 0) {
            rescuers.setT(i_rep, pop.getTg(imaxr));
            rescuers.setR(i_rep, pop.getrg(imaxr));
            rescuers.setNM(i_rep, static_cast<unsigned long>(pop.getnm(imaxr)));
          } else {
            rescuers.setT(i_rep, param.getT_max());
            rescuers.setR(i_rep, 0.0);
            rescuers.setNM(i_rep, 0);
          }

          rescuers.setNPC(i_rep, pop.getNpc());
      }
  }

  if(verbose) {
      std::cout << "running : " << 100 << "%" << std::endl;
  }

  if(save_output_simulation) {

    std::ofstream outfile;

    const std::string pathFile = output_path
      + "No" + std::to_string(param.getN0())
      + "_n" + std::to_string(param.getN())
      + "_ne" + std::to_string(param.getNe())
      + "_U" + std::to_string(param.getMu())
      + "_Es" + std::to_string(param.getEs())
      + "_so" + std::to_string(param.getSo())
      + "_rescuers";

      outfile.open(pathFile);
      outfile.clear();

      std::cout << "Save File : " << pathFile << std::endl;

      unsigned long rate = 0;

      outfile << "list(c(" << rescuers.getP(0);
      for (unsigned long i = 1; i < param.getN_rep(); i++) {
        outfile << ", " <<  rescuers.getP(i);
        if(rescuers.getP(i) > 0) {
          rate++;
        }
      }

      std::cout << "Rate : " << rate << "/"<< param.getN_rep() << " -> " << ((double)rate)/((double)param.getN_rep())*100 << "%" << std::endl;

      std::vector<double> rateNotNull;

      outfile << "), \nc(" << rescuers.getR(0);
      for (unsigned long i = 1; i < param.getN_rep(); i++) {
        outfile << ", " << rescuers.getR(i);
        if(rescuers.getR(i) > 0) {
          rateNotNull.push_back(rescuers.getR(i));
        }
      }

      outfile << "), \nc(" << rescuers.getT(0);
      for (unsigned long i = 1; i < param.getN_rep(); i++) {
        outfile << ", " << rescuers.getT(i);
      }

      outfile << "), \nc(" << rescuers.getNM(0);
      for (unsigned long i = 1; i < param.getN_rep(); i++) {
        outfile << ", " << rescuers.getNM(i);
      }

      outfile << "), \nc(" << rescuers.getNPC(0);
      for (unsigned long i = 1; i < param.getN_rep(); i++) {
        outfile << ", " << rescuers.getNPC(i);
      }

      outfile << "))";

      outfile.close();

      if(have_rate) {
        const std::string pathFileRate = pathFile+"_rate";

        outfile.open(pathFileRate);
        outfile.clear();

        outfile << "list(c(" << rateNotNull[0];
        for (unsigned long i = 1; i < rate; i++) {
          outfile << ", " <<  rateNotNull[i];
        }
        outfile << "))";

        outfile.close();
      }
  } else {
      unsigned long rate = 0;

      for (unsigned long i = 1; i < param.getN_rep(); i++) {
        if(rescuers.getP(i) > 0) {
          rate++;
        }
      }

      std::cout << "Rate : " << rate << "/"<< param.getN_rep() << " -> " << ((double)rate)/((double)param.getN_rep())*100 << "%" << std::endl;

  }

  List result = List::create(
    Named( "P" ) = rescuers.getVectorP(param.getN_rep()),
    Named( "R" ) = rescuers.getVectorR(param.getN_rep()),
    Named( "T" ) = rescuers.getVectorT(param.getN_rep()),
    Named( "NM" ) = rescuers.getVectorNM(param.getN_rep()),
    Named( "NPC" ) = rescuers.getVectorNPC(param.getN_rep())
  );

  return result;

}

int main(int argc, char *argv[]) {

    InputParser input(argc, argv);

    if(input.cmdOptionExists("-h") || input.cmdOptionExists("--help")){
        Utils::showUsage();
    }

    std::string file;
    if(input.cmdOptionExists("-i")) {
        file = input.getCmdOption("-i");
    } else {
        Utils::showUsage();
    }

    std::string output;
    bool save_output_simulation = true;
    if(input.cmdOptionExists("-o")) {
        output = input.getCmdOption("-o");
    } else {
        save_output_simulation = false;
    }

    unsigned long MAX_GENO = 500;
    if(input.cmdOptionExists("-g")) {
        MAX_GENO = std::stoul(input.getCmdOption("-g"));
    }

    std::string matrix;
    if(input.cmdOptionExists("-m")) {
        matrix = input.getCmdOption("-m");
    } else {
        Utils::showUsage();
    }

    bool have_rate = false;
    if(input.cmdOptionExists("-r")) {
        have_rate = true;
    }

    const bool SAVE_DYN = false;

    std::cout << "Start" << std::endl;

    Parameters param(SAVE_DYN, file);

    GMatrix A(matrix+Utils::separator()+"matA.dat", param.getNa(), param.getN(), true);
    GMatrix Sb(matrix+Utils::separator()+"matSb.dat", param.getN(), param.getN(), false);
    GMatrix x0(matrix+Utils::separator()+"vectxo.dat", param.getN(), 1, false);

    GRandom grd;

    const unsigned long nrnt = param.getNb_time_steps() * param.getN_rep();

    Rescuers rescuers(param.getN_rep());

    for(unsigned long i_rep = 0; i_rep < param.getN_rep(); i_rep++) {

        std::cout << "i_rep: " << i_rep << std::endl;

        Population pop(MAX_GENO, param.getN());
        pop.setng(1);
        pop.setNg(0, param.getN0());
        pop.setTg(0, 0.0);
        pop.evalbirthrate(&Sb, param.getBmax(), &x0, param.getN(), 0);
        pop.setrg(0, pop.getBg(0) - param.getD());
        pop.setNp(param.getN0());
        pop.setNpc(0.0);
        pop.setBp(pop.getBg(0)*param.getN0());
        pop.setnm(0, 0);
        double Er = pop.compute_Er();

        // initialize times
        double t = 0.0; // current time
        unsigned int nb = 0; // number of birth since the last rescue test

        rescuers.setP(i_rep, 1);

        unsigned long imaxr = 0;

        // simulation loop on time
        while( t < param.getT_max()) {
            // total rate of events and time at which next event occurs
            double total_rate = pop.getBp() + param.getD() * pop.getNp();
            double dt = grd.exponential(total_rate);
            t += dt;

            double u = grd.flat(0.0, total_rate);

            if(u < pop.getBp()) {

                if (pop.getng() >= MAX_GENO) {
                    std::cout << "ERROR: max number of genotypes reached\nIncrease MAX_GENO constant\n" << std::endl;
                    //TODO error
                } else {
                    pop.birth(&grd, t, u, &A, param.getMu(), param.getNa(), &Sb, param.getBmax(), &x0, param.getN(), param.getD(), dt);
                    nb++;

                    if(nb > param.getNbt()) { // if nbt birth, test is there is a rescue
                        nb = 0;
                        Er = pop.compute_Er();
                        if(Er > 0) {
                            if(pop.getNp() * log(param.getD() / (Er+param.getD())) < param.getLog10Pextlim()) {
                                break;
                            }
                        }
                    }
                }

            } else {
                pop.death(&grd, u, param.getD(), param.getN(), dt);
                if(pop.getNp() == 0) {
                    rescuers.setP(i_rep, 0);
                    break;
                }
            }
        } // end of simulation loop on time

        // test if simul stops because t_max reached (in this case, failed simul)
        if (t >= param.getT_max()) {
            std::cout << "ERROR: t_max reached\nIncrease factsecuext parameter\n" << std::endl;
            //TODO error
            exit(1);
        }

        imaxr = pop.compute_imaxr(imaxr);

        if(pop.getNp() != 0) {
            rescuers.setT(i_rep, pop.getTg(imaxr));
            rescuers.setR(i_rep, pop.getrg(imaxr));
            rescuers.setNM(i_rep, static_cast<unsigned long>(pop.getnm(imaxr)));
        } else {
            rescuers.setT(i_rep, param.getT_max());
            rescuers.setR(i_rep, 0.0);
            rescuers.setNM(i_rep, 0);
        }

        rescuers.setNPC(i_rep, pop.getNpc());
    }

    if(save_output_simulation) {

        std::ofstream outfile;

        const std::string pathFile = output + Utils::separator()
                                     + "No" + std::to_string(param.getN0())
                                     + "_n" + std::to_string(param.getN())
                                     + "_ne" + std::to_string(param.getNe())
                                     + "_U" + std::to_string(param.getMu())
                                     + "_Es" + std::to_string(param.getEs())
                                     + "_so" + std::to_string(param.getSo())
                                     + "_rescuers";

        outfile.open(pathFile);
        outfile.clear();



        unsigned long rate = 0;

        outfile << "list(c(" << rescuers.getP(0);
        for (unsigned long i = 1; i < param.getN_rep(); i++) {
            outfile << ", " <<  rescuers.getP(i);
            if(rescuers.getP(i) > 0) {
                rate++;
            }
        }

        std::cout << "Rate : " << rate << "/"<< param.getN_rep() << " -> " << ((double)rate)/((double)param.getN_rep())*100 << "%" << std::endl;

        std::vector<double> rateNotNull;

        outfile << "), \nc(" << rescuers.getR(0);
        for (unsigned long i = 1; i < param.getN_rep(); i++) {
            outfile << ", " << rescuers.getR(i);
            if(rescuers.getR(i) > 0) {
                rateNotNull.push_back(rescuers.getR(i));
            }
        }

        outfile << "), \nc(" << rescuers.getT(0);
        for (unsigned long i = 1; i < param.getN_rep(); i++) {
            outfile << ", " << rescuers.getT(i);
        }

        outfile << "), \nc(" << rescuers.getNM(0);
        for (unsigned long i = 1; i < param.getN_rep(); i++) {
            outfile << ", " << rescuers.getNM(i);
        }

        outfile << "), \nc(" << rescuers.getNPC(0);
        for (unsigned long i = 1; i < param.getN_rep(); i++) {
            outfile << ", " << rescuers.getNPC(i);
        }

        outfile << "))";

        outfile.close();

        if(have_rate) {
            const std::string pathFileRate = pathFile+"_rate";

            outfile.open(pathFileRate);
            outfile.clear();

            outfile << "list(c(" << rateNotNull[0];
            for (unsigned long i = 1; i < rate; i++) {
                outfile << ", " <<  rateNotNull[i];
            }
            outfile << "))";

            outfile.close();
        }
    }

    std::cout << "Finish" << std::endl;

    return 0;

}
