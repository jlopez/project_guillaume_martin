//
// Created by jimmy on 03/10/17.
//

#include "GRandom.h"

#include <time.h>

GRandom::GRandom() {
    /*const gsl_rng_type *Tgsl;
    gsl_rng_env_setup();
    Tgsl = gsl_rng_default;
    m_rgsl = gsl_rng_alloc(Tgsl);
    gsl_rng_set (m_rgsl, (unsigned long) time (nullptr));*/
}

GRandom::~GRandom() {
    //delete m_rgsl;
}

double GRandom::exponential(double mu) {
    //return gsl_ran_exponential(m_rgsl, 1.0 / mu);
    std::exponential_distribution<double> distribution(mu);
    return distribution(m_generator);
}

double GRandom::flat(double a, double b) {
    //return gsl_ran_flat(m_rgsl, a, b);
    std::uniform_real_distribution<double> distribution(a,b);
    return distribution(m_generator);
}

unsigned long GRandom::poisson(double mu) {
    //return gsl_ran_poisson(m_rgsl, mu);
    std::poisson_distribution<int> distribution(mu);
    return distribution(m_generator);
}


