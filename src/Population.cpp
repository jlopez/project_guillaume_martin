//
// Created by jimmy on 02/10/17.
//

#include "Population.h"
#include "Utils.h"
#include <cmath>
#include <algorithm>

Population::Population(unsigned long rows, unsigned long cols) {
    m_matrix = new GMatrix(rows, cols);

    m_Bg.resize(rows);
    m_rg.resize(rows);
    m_Ng.resize(rows);
    m_Tg.resize(rows);
    m_nm.resize(rows);
}


Population::~Population() {
    delete m_matrix;
}

double Population::getBg(unsigned long index) {
    return m_Bg.at(index);
}

void Population::setBg(unsigned long index, double value) {
    m_Bg[index] = value;
}

double Population::getrg(unsigned long index) {
    return m_rg.at(index);
}

void Population::setrg(unsigned long index, double value) {
    m_rg[index] = value;
}

double Population::getMaxrg() {
    auto itm = std::max_element(std::begin(m_rg), std::end(m_rg));
    return *itm;
}

unsigned long Population::getNg(unsigned long index) {
    return m_Ng.at(index);
}

void Population::setNg(unsigned long index, unsigned long value) {
    m_Ng[index] = value;
}

double Population::getTg(unsigned long index) {
    return m_Tg.at(index);
}

void Population::setTg(unsigned long index, double value) {
    m_Tg[index] = value;
}

double Population::getnm(unsigned long index) {
    return m_nm.at(index);
}

void Population::setnm(unsigned long index, unsigned long value) {
    m_nm[index] = value;
}

double Population::getValue(unsigned long i, unsigned long j) {
    return m_matrix->getValue(i, j);
}

void Population::setValue(unsigned long i, unsigned long j, double value) {
    m_matrix->setValue(i, j, value);
}

double Population::getBp() const {
    return m_Bp;
}

void Population::setBp(double Bp) {
    m_Bp = Bp;
}

unsigned long Population::getNp() const {
    return m_Np;
}

void Population::setNp(unsigned long Np) {
    m_Np = Np;
}

double Population::getNpc() const {
    return m_Npc;
}

void Population::setNpc(double Npc) {
    m_Npc = Npc;
}

unsigned long Population::getng() const {
    return m_ng;
}

void Population::setng(unsigned long ng) {
    m_ng = ng;
}

double Population::compute_Er() {
    double Er = 0.0;

    if(m_Np != 0.0) {
        for (unsigned long i = 0; i < m_ng; ++i) {
            Er += m_rg[i] * m_Ng[i];
        }
        Er /= m_Np;
    }

    return Er;
}

void Population::evalbirthrate(GMatrix *Sb, double BMax, const GMatrix *x0, unsigned long n, unsigned long nl) {

    GMatrix x(n, 1);


    for(unsigned long i = 0; i < n; i++) {
        x.setValue(i, 0, m_matrix->getValue(nl, i));
    }

    x.sub(x0);

    GMatrix xtmp(1, n);
    GMatrix xscal(1, 1);

    Utils::dgemmTN(&x, Sb, &xtmp);
    Utils::dgemmNN(&xtmp, &x, &xscal);

    double dum = BMax - xscal.getValue(0, 0) / 2.0;

    if(dum > 0) {
        m_Bg[nl] = dum;
    } else {
        m_Bg[nl] = 0.0;
    }
}

void
Population::birth(GRandom *rnd, double t, double u, GMatrix *A, double mu, unsigned long na, GMatrix *Sb, double Bmax,
                  GMatrix *x0, unsigned long n, double d, double dt) {

    // index of the genotype of the parent
    unsigned long kg = 0;
    double sum = m_Bg[0] * m_Ng[0];

    while(sum <= u) {
        kg++;
        sum += m_Bg[kg] * m_Ng[kg];
    }

    // nb of mutation that the new indiv carry
    unsigned long nm = rnd->poisson(mu);

    if(nm > 0) {
        // compute the sum of the effects of each mutation

        // vector of the index of the alleles produced by mutation
        unsigned long ma[nm];

        for (unsigned long i = 0; i < nm; i++) {
            ma[i] = static_cast<unsigned long>(floor(rnd->flat(0.0, static_cast<double>(na))));
        }

        double dz[n]; // vector of mutation effect

        for (unsigned long i = 0; i < n; ++i) {
            dz[i] = 0;

            for (unsigned long j = 0; j < nm; j++) {
                dz[i] += A->getValue(ma[j], i);
            }
        }

        // define properties of the new individual
        // (ng, before increased, is the index of genotype of the mutant)
        for (unsigned long i = 0; i < n; i++) {
            setValue(m_ng, i, getValue(kg, i)+dz[i]);
        }

        m_Ng[m_ng] = 1;
        m_Tg[m_ng] = t;

        evalbirthrate(Sb, Bmax, x0, n, m_ng);

        m_nm[m_ng] = m_nm[kg] + 1;
        m_rg[m_ng] = m_Bg[m_ng] - d;

        // update population properties
        m_Bp += m_Bg[m_ng];
        m_Np++;
        m_Npc += m_Np * dt;
        m_ng++;

    } else {
        m_Ng[kg]++;
        m_Np++;
        m_Npc += m_Np * dt;
        m_Bp += m_Bg[kg];
    }

}

void Population::death(GRandom *rnd, double u, const double d, unsigned long n, double dt) {

    // index of the indiv to kill (kd=0 for the first indiv)
    auto ki = static_cast<unsigned long>(floor((u - m_Bp)/d));

    unsigned long kg = 0;
    unsigned long sum = m_Ng[kg];

    while(sum <= ki) {
        sum += m_Ng[++kg];
    }

    m_Ng[kg]--;
    m_Np--;
    m_Npc += m_Np * dt;
    m_Bp -= m_Bg[kg];

    // if there is no more individual of genotype kg, then delete the line kg of the pop
    // and replace it by the last line of the table (to avoid lines empty of individuals)

    if(m_Ng[kg] == 0) {
        // update nb of existing genotypes
        // this new ng is the index of the last line of genotypes (cause starts at 0)
        m_ng--;
        m_Bg[kg] = m_Bg[m_ng];
        m_rg[kg] = m_rg[m_ng];
        m_Ng[kg] = m_Ng[m_ng];
        m_Tg[kg] = m_Tg[m_ng];
        m_nm[kg] = m_nm[m_ng];

        for (unsigned long i = 0; i < n; i++) {
            setValue(kg, i, getValue(m_ng, i));
        }
    }
}

unsigned long Population::compute_imaxr(unsigned long max) {
    unsigned long imaxr = 0;

    // if there is a positive growth rate
    if(getMaxrg() > 0) {
        // then take the one of the most frequent genotype
        while(m_rg[imaxr] <= 0) {
            imaxr++;
        }
        for (unsigned long i = imaxr; i < m_ng; i++) {
            if(m_rg[i] > 0 && m_Ng[i] > m_Ng[imaxr]) {
                imaxr = i;
            }
        }
    } else {
        for (unsigned long i = imaxr; i < m_ng; i++) {
            if(m_Ng[i] > m_Ng[imaxr]) {
                imaxr = i;
            }
        }
    }

    return imaxr;
}







